import './SideVideoThumbnail.css';
import VideoThumbnailData from '../../../classes/videoThumbnailData';
import { useEffect } from "react"
import moment from 'moment';

function SideVideoThumbnail(props: any) {
  let video: VideoThumbnailData = props.video

  let maxLen: number = 42
  if(video.title.length > maxLen){
    video.title = video.title.slice(0, maxLen) + "..."
  }

  useEffect(() => {
    let date: Date = new Date(video.date_posted)
    video.date_posted = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()))
  }, [])

  return (
    <div className="VideoPreview col-span-1 flex">
        <img src={video.thumbnail_url} alt="" className="SideThumbnail aspect-video object-cover rounded"/>
        <div className="flex flex-col">
            <div className="ml-3 max-w-[250px]">
                <b className="VideoTitle text-md">{video.title}</b>
                <p className="ChannelName text-sm text-stone-300">John Doe</p>
                <div className='flex items-center gap-1.5'>
                    <p className="ViewCount text-sm text-stone-300">{video.views} views</p>
                    <div className='bg-stone-300 aspect-square h-1 rounded-full'></div>
                    <p className="ViewCount text-sm text-stone-300">{moment.utc(video.date_posted).fromNow()}</p>
                  </div>
            </div>
        </div>
    </div>
  );
}

export default SideVideoThumbnail;
