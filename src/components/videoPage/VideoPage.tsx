import './VideoPage.css';
import { useEffect, useState, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { AiOutlineLike, AiOutlineDislike } from "react-icons/ai"
import axios from "axios"
import moment from "moment"

import VideoData from '../../classes/videoData';
import SideVideoThumbnail from './sideVideoThumbnail/SideVideoThumbnail';
import CommentSection from './commentSection/CommentSection';
import VideoThumbnailData from '../../classes/videoThumbnailData';

import videojs from 'video.js';
import VideoJS from './videoPlayer/VideoJS';

function VideoPage(props: any) {
  const navigate = useNavigate()

  const [video, setVideo] = useState(new VideoData())
  const [sideVideos, setSideVideos] = useState<VideoThumbnailData[]>([])
  
  let [descriptionOpen, setDescriptionOpen] = useState(false)
  const toggleDescription = () => {
    setDescriptionOpen(!descriptionOpen)
  }

  let splitUrl = window.location.href.split("/")
  let videoID = splitUrl[splitUrl.length-1]
  let apiUrl = `${process.env.REACT_APP_VIDEO_API_BASE_URL}/videos/${videoID}`

  useEffect(() => {
    if(props.hamburgerMenuOpen) {
        props.toggleHamburgerMenu()
    }

    axios.get(apiUrl).then((response) => {
        let videoData: VideoData = response.data
        let date: Date = new Date(videoData.date_posted)
        videoData.date_posted = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()))
        setVideo(videoData)
    })

    axios.get(`${process.env.REACT_APP_VIDEO_API_BASE_URL}/videos/thumbnails`).then((response) => {
        setSideVideos(response.data)
    })
  }, [])

  const playerRef = useRef(null);

  let videoJsOptions = {
    autplay: true,
    controls: true,
    responsive: true,
    fluid: true,
    sources: [{
      src: video.video_url,
      //src: 'http://res.cloudinary.com/dguxtsd45/raw/upload/v1681930107/illuminize/videos/2cac688d-1fa3-45ce-88c1-81f007604ab7/playlist.m3u8',
      type: 'application/x-mpegURL'
    }]
  }

  const handlePlayerReady = (player: any) => {
    playerRef.current = player;

     // You can handle player events here, for example:
    player.on('waiting', () => {
      videojs.log('player is waiting');
    });

    player.on('dispose', () => {
      videojs.log('player will dispose');
    });
  }

  return (
    <div className="mt-20 flex gap-12">
        <div className="pl-16 grow">
            <VideoJS options={videoJsOptions}/>
            <div className="mt-3">
                <b className="text-3xl">{video.title}</b>
                <div className="flex mt-3 h-10 items-center">
                    <Link to="/profile/1">
                        <img src="/images/default_profile.png" alt="" className="aspect-square object-cover h-10 rounded-full"/>
                    </Link>
                    <div className="flex flex-col justify-center ml-3">
                        <Link to="/profile/1" className="text-md font-bold">John Doe</Link>
                        <p className="text-xs text-stone-300">100 Subscribers</p>
                    </div>
                    <button className='font-semibold bg-white text-stone-900 text-sm rounded-full px-3 py-2 ml-5 hover:bg-gray-200'>Subscribe</button>
                    <Link to={`/edit/video/${videoID}`} className='font-semibold bg-blue-600 text-white text-sm rounded-full px-3 py-2 ml-2 hover:bg-blue-500'>Edit Video</Link>
                    <div className="grow"></div>
                    <div className='bg-stone-700 h-full flex items-center pl-3 gap-1 rounded-l-full hover:bg-stone-600 hover:cursor-pointer select-none'>
                        <AiOutlineLike className='text-white w-6 h-6'/>
                        <p className='font-semibold text-sm'>200</p>
                        <div className='h-8 w-[1px] bg-stone-400 ml-2'></div>
                    </div>
                    <div className='bg-stone-700 h-full flex items-center px-3 gap-1 rounded-r-full hover:bg-stone-600 hover:cursor-pointer select-none'>
                        <AiOutlineDislike className='text-white w-6 h-6'/>
                        <p className='font-semibold text-sm'>10</p>
                    </div>
                </div>
                <div className="bg-stone-800 rounded-md p-2 mt-4">
                    <div className='flex gap-3'>
                        <p className='font-semibold text-sm'>{video.views} views</p>
                        <p className='font-semibold text-sm'>{moment.utc(video.date_posted).fromNow()}</p>
                    </div>
                    <textarea disabled={true} rows={descriptionOpen ? 10 : 2} value={video.description} className={`text-sm text-white w-full mt-1 resize-none bg-stone-800 ${descriptionOpen ? "overflow-visible" : "overflow-hidden"}`}></textarea>
                    <button className='text-sm font-bold mt-1' onClick={toggleDescription}>{descriptionOpen ? <p>Show less</p> : <p>Show more</p>}</button>
                </div>
            </div>
            <CommentSection/>
        </div>

        <div className="row flex flex-col gap-4 min-w-fit mr-16">
        {sideVideos.map((video, index) => {
            return <SideVideoThumbnail video={video} key={index}/>
          })}
        </div>
    </div>
  );
}

export default VideoPage;
