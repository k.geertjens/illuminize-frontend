import './CommentSection.css';

import CommentData from '../../../classes/commentData';
import Comment from './comment/Comment';

function CommentSection() {
  let comments = [
    new CommentData("John Doe", "2 days ago", "abc"),
    new CommentData("Jane Johnson", "6 hours ago", "yes"),
    new CommentData("Jimmy McGill", "1 hour ago", "It's all good man!"),
    new CommentData("Walter Hartwell White", "3 months ago", "Very cool video :)"),
  ]
  return (
    <div className="flex flex-col gap-5 mt-5">
      <div className='w-full flex gap-2'>
        <img src="/images/default_profile.png" alt="" className='aspect-square h-10 rounded-full object-cover'/>
        <input type="text" name="comment" id="comment" placeholder="Leave a comment..." className='bg-stone-900 ml-2 py-1 grow border-b-2 border-stone-700 text-sm'/>
      </div>
        {comments.map((comment, index) => {
            return <Comment key={index} comment={comment}/>
        })}
    </div>
  );
}

export default CommentSection;
