import './Comment.css';
import { Link } from 'react-router-dom';
import CommentData from '../../../../classes/commentData';


function Comment(props: any) {
  let comment: CommentData = props.comment

  return (
    <div className="w-full flex gap-2">
        <Link to="/profile/1" className='min-w-fit'>
          <img src="/images/default_profile.png" alt="" className='aspect-square h-10 object-cover rounded-full'/>
        </Link>
        <div className='flex flex-col grow ml-2'>
            <div className='flex gap-1 items-center'>
                <Link to="/profile/1" className='text-sm font-semibold'>{comment.name}</Link>
                <p className='text-xs text-stone-300'>{comment.date}</p>
            </div>
            <p className='text-sm'>{comment.text}</p>
        </div>
    </div>
  );
}

export default Comment;
