import './UploadPage.css';
import { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import { RiFileUploadLine } from "react-icons/ri";
import { toast } from "react-hot-toast"
import axios from 'axios';
import VideoPostRequest from '../../classes/videoPostRequest';
import {AiOutlineCheck} from "react-icons/ai"
import { useAuth0 } from "@auth0/auth0-react";


function UploadPage(props: any) {

  const { user, isAuthenticated, getAccessTokenSilently } = useAuth0();

  useEffect(() => {
    if(props.hamburgerMenuOpen){
        props.toggleHamburgerMenu()
    }
  })

  const navigate = useNavigate()
  const [dragActive, setDragActive] = useState(false)

  const handleDrag = (e: any) => {
    e.preventDefault()
    e.stopPropagation()

    if (e.type === "dragenter" || e.type === "dragover") {
      setDragActive(true)
    } 
    else if (e.type === "dragleave") {
      setDragActive(false)
    }
  }

  const [videoUrl, setVideoUrl] = useState("")
  const [title, setTitle] = useState("")
  const [description, setDescription] = useState("")

  const handleTitleChange = (e: any) => {
    setTitle(e.target.value)
  }
  const handleDescriptionChange = (e: any) => {
    setDescription(e.target.value)
  }

  const [videoFileUploaded, setVideoFileUploaded] = useState(false)

  const handleDrop = (e: any) => {
    e.preventDefault()
    e.stopPropagation()

    setDragActive(false)
    let file = e.dataTransfer.files[0]
    if (file) {
      if(file.type.includes("video")){
        uploadFile(file)
      }
      else{
        toast.error("File must be a video")
      }
    }
  }

  const handleChange = (e: any) => {
    e.preventDefault()

    let file = e.dataTransfer.files[0]
    if (file) {
      if(file.type.includes("video")){
        uploadFile(file)
      }
      else{
        toast.error("File must be a video")
      }
    }
  }

  const uploadFile = (file: File) => {
    let data: FormData = new FormData()
    data.append("file", file)
    setVideoFileUploaded(true)
    axios.post(`${process.env.REACT_APP_VIDEO_PROCESSOR_BASE_URL}/video`, data, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    }).then((response) => {
      setVideoUrl(response.data)
    })
  }

  const handlePublish = () => {
    if(videoUrl != ""){
      let video: VideoPostRequest = new VideoPostRequest()
      video.poster_id = 1
      video.video_url = videoUrl
      video.thumbnail_url = "https://i.imgur.com/Gs035nD.jpeg"
      video.title = title
      video.description = description
      console.log(video)

      postVideo(video)
    }
    else{
      toast.error("Video file has not finished uploading yet")
    }
  }

  const postVideo = (video: VideoPostRequest) => {
    axios.post(`${process.env.REACT_APP_VIDEO_API_BASE_URL}/videos`, video).then((response) => {
      toast.success("Video published successfully")
      let id = response.data
      console.log(response)
      navigate(`/video/${id}`)
    })
  }

  let uploadForm = <form action="" id="file-upload" onDragEnter={handleDrag} onSubmit={(e) => e.preventDefault()} className={`w-2/3 flex flex-col h-96 rounded-md mt-6 px-4 py-3 relative ${dragActive ? 'bg-stone-700 ring-2 ring-sky-600' : 'bg-stone-800'}`}>
                      <h1 className='text-white text-2xl font-semibold'>Upload New Video</h1>
                      <div className='flex flex-col gap-3 items-center justify-center grow m-5'>
                          <RiFileUploadLine className='text-white h-20 w-20'/>
                          <p>Drag the video file you want to upload here</p>
                          <label htmlFor="file-upload-input" className='my-4 bg-blue-500 text-white font-semibold text-md px-3 py-2 hover:bg-blue-700 hover:cursor-pointer'>Select File</label>
                          <input type="file" accept="video/*" id="file-upload-input" className='hidden' onChange={handleChange}></input>
                      </div>
                    {dragActive ? <div id="drag-file-element" className="w-full h-full absolute top-0 left-0" onDragEnter={handleDrag} onDragLeave={handleDrag} onDragOver={handleDrag} onDrop={handleDrop}></div> : <></>}
                    </form>

  let videoDetails =  <div className='w-2/3 flex flex-col justify-between h-96 rounded-md mt-6 px-4 py-3 bg-stone-800'>
                        <div className='flex flex-col gap-2'>
                          <h1 className='text-white text-2xl font-semibold mb-2'>Upload New Video</h1>
                          <label htmlFor="title" className='text-white text-sm'>Title:</label>
                          <input type="text" name="title" id="title" placeholder='Title...' className='text-sm px-2 py-2 bg-stone-800 ring-1 ring-stone-600 rounded-md' onChange={handleTitleChange}></input>
                          <label htmlFor="description" className='text-white text-sm mt-2'>Description:</label>
                          <textarea name="description" id="description" placeholder='Description' rows={4} className='text-white text-sm px-2 py-2 bg-stone-800 ring-1 ring-stone-600 rounded-md resize-none' onChange={handleDescriptionChange}></textarea>
                        </div>
                        <div className='flex items-center justify-between'>
                          <div className='flex gap-1 items-center'>
                            {videoUrl == "" ? <div className='loading-spinner mr-1'/> : <></>}
                            {videoUrl == "" ? <></> : <AiOutlineCheck className='text-white'/>}
                            <p className='font-semibold'>{videoUrl == "" ? "Uploading..." : "Upload Completed"}</p>
                          </div>
                          <button  disabled={videoUrl==""} className={`px-2 py-1 ${videoUrl == "" ? "bg-stone-500 text-stone-200" : "bg-blue-600 hover:bg-blue-500"} rounded-md`} onClick={handlePublish}>Publish</button>
                        </div>
                      </div>

  return (
    <div className="flex flex-col items-center mt-20">
        {videoFileUploaded ? videoDetails : uploadForm }
    </div>
  );
}

export default UploadPage;
