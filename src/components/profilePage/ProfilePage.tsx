import './ProfilePage.css';
import { useState, useEffect } from "react";
import { RiFileUploadLine } from "react-icons/ri";
import { toast } from "react-hot-toast"
import VideoList from '../frontPage/videoList/VideoList';
import VideoThumbnail from '../frontPage/videoList/videoThumbnail/VideoThumbnail';
import VideoThumbnailData from '../../classes/videoThumbnailData';
import axios from 'axios';
import {AiOutlineCheck} from "react-icons/ai"


function ProfilePage(props: any) {

    useEffect(() => {
        if(props.hamburgerMenuOpen){
            props.toggleHamburgerMenu()
        }
    }, [])

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_VIDEO_API_BASE_URL}/videos/thumbnails`, {
          params: {searchString: props.search}
        }).then((response) => {
          setVideos(response.data)
      })
      }, [props])

    const [videos, setVideos] = useState<VideoThumbnailData[]>([])
    
    const [subscribed, setSubscribed] = useState(false)
    const toggleSubscribe = () => {
        if(!subscribed){ toast.success("Subscribed to John Doe") }
        else{ toast.success("Unsubscribed from John Doe") }
        setSubscribed(!subscribed)
    }

    let subscribeButton = <button className='bg-blue-600 px-2 py-1 rounded-md font-semibold hover:bg-blue-500' onClick={toggleSubscribe}>Subscribe</button>
    let subscribedButton =  <button className='bg-stone-600 px-2 py-1 rounded-md font-semibold hover:bg-stone-500 flex gap-0.5 items-center'onClick={toggleSubscribe}>
                                {<AiOutlineCheck/>}Subscribed
                            </button>

    return (
        <div className="flex flex-col items-center mt-20 px-28 w-full">
            <div className='w-full py-5 flex items-center gap-5 sticky'>
                <img src="/images/default_profile.png" className="w-24 h-24 aspect-square object-cover rounded-full select-none"/>
                <div className='flex flex-col grow'>
                    <div className='flex justify-between items-center'>
                        <p className='font-semibold text-2xl'>John Doe</p>
                        <div className='flex gap-2'>
                            {subscribed ? subscribedButton : subscribeButton}
                            <button className='bg-stone-600 px-2 py-1 rounded-md font-semibold hover:bg-stone-500'>Edit Profile</button>
                        </div>
                    </div>
                    <div className='flex gap-2 items-center'>
                        <p className='text-sm text-stone-300'>10 subscribers</p>
                        <div className='bg-stone-300 h-1.5 w-1.5 rounded-full'/>
                        <p className='text-sm text-stone-300'>20 videos</p>
                    </div>
                    <p className='mt-2 text-sm text-stone-300'>A more detailed description of this channel...</p>
                </div>
            </div>
            <div className='bg-stone-400 w-full h-[1px] my-2'/>
            <div className=''>
                <p className='font-bold text-xl my-4'>Videos</p>
                <div className='ProfileVideoList grid gap-x-12 gap-y-12'>
                    {videos.map((video, index) => {
                        return <VideoThumbnail key={index} video={video}/>
                    })}
                </div>
            </div>
        </div>
    );
}

export default ProfilePage;
