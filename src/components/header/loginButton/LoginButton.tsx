import { useAuth0 } from "@auth0/auth0-react";

const LoginButton = () => {
  const { loginWithRedirect } = useAuth0();

  return <button onClick={() => loginWithRedirect()} className="pl-4 py-1 hover:bg-stone-600 text-left">Log In</button>;
};

export default LoginButton;