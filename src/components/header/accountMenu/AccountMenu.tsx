import { Link, useSearchParams } from 'react-router-dom';
import './AccountMenu.css';

import MenuButtonData from '../../../classes/menuButtonData';
import LoginButton from '../loginButton/LoginButton';
import LogoutButton from '../logoutButton/LogoutButton';

import { useAuth0 } from "@auth0/auth0-react";
import { useEffect, useState } from "react"

function AccountMenu(props: any) {

  const { user, isAuthenticated, isLoading } = useAuth0();

  let buttons: MenuButtonData[] = [
    new MenuButtonData("Your Profile", "/profile/1", <></>),
  ]

  let username: string | undefined
  username = (typeof user === 'undefined') ? "Not logged in" : user.name

  return (
    <div className="bg-stone-800 w-52 h-52 fixed top-16 right-0 flex flex-col ">
        <b className='px-4 py-3 text-lg'>{username}</b>
        {isAuthenticated ? 
        buttons.map((button, index) => {
            return <Link to={button.link} key={index} className="pl-4 py-1 hover:bg-stone-600"  onClick={props.closeMenu}><p>{button.text}</p></Link>
        })
        : <></>}
        {isAuthenticated ? <></> : <LoginButton/>}
        {isAuthenticated ? <LogoutButton/> : <></>}
    </div>
  );
}

export default AccountMenu;
