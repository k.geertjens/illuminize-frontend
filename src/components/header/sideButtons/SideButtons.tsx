import './SideButtons.css';
import { useState } from 'react';
import {RiVideoAddLine} from "react-icons/ri"

import AccountMenu from '../accountMenu/AccountMenu';
import { Link } from 'react-router-dom';

function SideButtons() {
  let [menuVisible, setMenuVisible] = useState(false)

  const toggleMenuVisible = () => {
    setMenuVisible(!menuVisible)
  }

  const closeMenu = () => {
    setMenuVisible(false)
  }

  return (
    <>
        <div className="flex justify-end items-center gap-5 w-56">
            <Link to="/upload" className='w-11 h-11 p-2 rounded-full hover:bg-stone-600 hover:cursor-pointer flex justify-center items-center'>
                <RiVideoAddLine className="text-white w-8 h-8"/>
            </Link>
            <img src="/images/default_profile.png" alt="" className="ProfileIcon w-11 mr-6 aspect-square object-cover rounded-full hover:cursor-pointer select-none" onClick={toggleMenuVisible}/>
        </div>
        {menuVisible ? <AccountMenu closeMenu={closeMenu}/> : <></>}
    </>
  );
}

export default SideButtons;
