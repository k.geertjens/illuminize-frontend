import './Logo.css';
import { AiOutlinePlayCircle } from "react-icons/ai";
import { Link } from 'react-router-dom';

function Logo() {
  return (
    <Link to="/" className="Logo flex items-center gap-2 w-52">
      <AiOutlinePlayCircle className="text-blue-500 w-8 mt-0.5 h-auto"/>
      <h1 className="text-3xl select-none">Illuminize</h1>
    </Link>
  );
}

export default Logo;
