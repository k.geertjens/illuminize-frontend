import './MenuButton.css';
import { Link } from 'react-router-dom';
import MenuButtonData from '../../../../classes/menuButtonData';

function MenuButton(props: any) {
  let button: MenuButtonData = props.button
  let open: boolean = props.open
  
  let openDisplay = <Link to={button.link} className="w-full h-6 py-5 px-2 -ml-2 flex items-center gap-4 rounded-lg hover:bg-stone-600">
                      {button.icon}
                      <p className="text-lg font-medium select-none">{button.text}</p>
                    </Link>

  let closedDisplay = <Link to={button.link} className="w-full -ml-2 py-2 flex flex-col items-center rounded-lg hover:bg-stone-600">
                        {button.icon}
                        <p className="text-xs font-medium select-none">{button.text}</p>
                      </Link>

  return (
    <>
      {open ? openDisplay : closedDisplay}
    </>
  );
}

export default MenuButton;
