import './HamburgerMenu.css';
import {AiOutlineHome, AiOutlinePlaySquare, AiOutlineLike, AiOutlineHistory} from "react-icons/ai"

import MenuButton from './menuButton/MenuButton';
import MenuButtonData from '../../../classes/menuButtonData';


function HamburgerMenu(props: any) {
  let open: boolean = props.open

  let topButtons: MenuButtonData[] = [
    new MenuButtonData("Home", "/", <AiOutlineHome className="text-white w-5 h-5"/>),
    new MenuButtonData("Subscriptions", "/", <AiOutlinePlaySquare className="text-white w-5 h-5"/>),
  ]

  let mainButtons: MenuButtonData[] = [
    new MenuButtonData("Liked Videos", "/", <AiOutlineLike className="text-white w-5 h-5"/>),
    new MenuButtonData("History", "/", <AiOutlineHistory className="text-white w-5 h-5"/>)
  ]

  let openDisplay = <div className="h-screen pl-5 w-60 flex flex-col fixed left-0 top-20 bg-stone-900">
                      <div className='h-4 w-full bg-stone-900'></div>
                      <div className='bg-stone-400 w-full h-[1px] mb-2'></div>
                      {topButtons.map((button, index) => {
                        return <MenuButton key={index} button={button} open={open}/>
                      })}
                      <div className='bg-stone-400 w-full h-[1px] my-3'></div>
                      {mainButtons.map((button, index) => {
                        return <MenuButton key={index} button={button} open={open}/>
                      })}
                      <div className='bg-stone-400 w-full h-[1px] mt-2'></div>
                      <div className='w-full bg-stone-900 h-96'></div>
                    </div>
  return (
    <>
      {open ? openDisplay : <></>}
    </>
  );
}

export default HamburgerMenu;
