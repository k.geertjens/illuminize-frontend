import './Searchbar.css';
import { AiOutlineSearch } from "react-icons/ai"

function Searchbar(props: any) {
  return (
    <div className="Searchbar flex justify-center items-center grow h-9">
        <div className="flex items-center gap-1 h-full px-2 grow max-w-4xl rounded-lg bg-stone-600 focus-within:ring-1 ring-sky-600">
            <AiOutlineSearch className="text-white"/>
            <input type="text" name="search" id="search" placeholder="Search..." className="bg-stone-600 grow" onInput={props.setSearch}/>
        </div>
    </div>
  );
}

export default Searchbar;
