import './Header.css';
import {GiHamburgerMenu} from "react-icons/gi"

import Searchbar from './searchbar/Searchbar';
import Logo from './logo/Logo';
import SideButtons from './sideButtons/SideButtons';
import HamburgerMenu from './hamburgerMenu/HamburgerMenu';

function Header(props: any) {
  let hamburgerMenuOpen = props.hamburgerMenuOpen

  return (
    <div className="Header flex items-center gap-5 w-full h-20 justify-between fixed z-10 bg-stone-900">
      <div className='w-11 h-11 p-2 ml-3 rounded-full hover:bg-stone-600 hover:cursor-pointer flex justify-center items-center' onClick={props.toggleHamburgerMenu}>
        <GiHamburgerMenu className="text-white w-8 h-8"/>
      </div>
      <HamburgerMenu open={hamburgerMenuOpen}/>

      <Logo/>
      <Searchbar setSearch={props.setSearch}/>
      <SideButtons/>
    </div>
  );
}

export default Header;
