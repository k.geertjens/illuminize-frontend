import './App.css';

import { Route, Routes, BrowserRouter } from "react-router-dom"
import { useState } from "react"
import { Toaster } from "react-hot-toast"

import Header from './header/Header';
import FrontPage from './frontPage/FrontPage';
import VideoPage from './videoPage/VideoPage';
import VideoEditPage from './videoEditPage/VideoEditPage';
import UploadPage from './uploadPage/UploadPage';
import ProfilePage from './profilePage/ProfilePage';

function App() {
  let [hamburgerMenuOpen, setHamburgerMenuOpen] = useState(true)
  const toggleHamburgerMenu = () => {
    setHamburgerMenuOpen(!hamburgerMenuOpen)
  }

  const [search, setSearch] = useState("")
  const handleSearchInput = (event: any) => {
    setSearch(event.target.value)
  }

  return (
    <div className="App min-h-screen flex flex-col bg-stone-900">
      <Toaster position="top-center" />
      <BrowserRouter>
        <Header toggleHamburgerMenu={toggleHamburgerMenu} hamburgerMenuOpen={hamburgerMenuOpen} setSearch={handleSearchInput}/>
        <Routes>
          <Route path="/" element={<FrontPage toggleHamburgerMenu={toggleHamburgerMenu} hamburgerMenuOpen={hamburgerMenuOpen} search={search}/>}/>
          <Route path="/video/*" element={<VideoPage toggleHamburgerMenu={toggleHamburgerMenu} hamburgerMenuOpen={hamburgerMenuOpen}/>}/>
          <Route path="/edit/video/*" element={<VideoEditPage toggleHamburgerMenu={toggleHamburgerMenu} hamburgerMenuOpen={hamburgerMenuOpen}/>}/>
          <Route path="/upload" element={<UploadPage toggleHamburgerMenu={toggleHamburgerMenu} hamburgerMenuOpen={hamburgerMenuOpen}/>}/>
          <Route path="/profile/*" element={<ProfilePage toggleHamburgerMenu={toggleHamburgerMenu} hamburgerMenuOpen={hamburgerMenuOpen}/>}/>
        </Routes>
        <div className='footer w-full h-10'></div>
      </BrowserRouter>
    </div>
  );
}

export default App;
