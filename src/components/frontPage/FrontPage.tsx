import './FrontPage.css';
import { useEffect } from 'react';

import VideoList from './videoList/VideoList';

function FrontPage(props: any) {
  useEffect(() => {
    if(!props.hamburgerMenuOpen) {
        props.toggleHamburgerMenu()
    }
  }, [])

  return (
    <div className="mt-24 flex max-h-min" >
      <VideoList hamburgerMenuOpen={props.hamburgerMenuOpen} search={props.search}/>
      <div className='w-8'></div>
    </div>
  );
}

export default FrontPage;
