import VideoThumbnailData from '../../../classes/videoThumbnailData';
import './VideoList.css';
import { useEffect, useState } from 'react'
import axios from 'axios';

import VideoThumbnail from './videoThumbnail/VideoThumbnail';

function VideoList(props: any) {
  const [videos, setVideos] = useState<VideoThumbnailData[]>([])

  useEffect(() => {
    axios.get(`${process.env.REACT_APP_VIDEO_API_BASE_URL}/videos/thumbnails`, {
      params: {searchString: props.search}
    }).then((response) => {
      setVideos(response.data)
  })
  }, [props])

  return (
    <div className={`VideoList w-full px-12 grid gap-x-12 gap-y-12 ${props.hamburgerMenuOpen ? 'ml-60' : 'ml-10'}`}>
      {videos.map((video, index) => {
        return <VideoThumbnail key={index} video={video}/>
      })}
    </div>
  );
}

export default VideoList;
