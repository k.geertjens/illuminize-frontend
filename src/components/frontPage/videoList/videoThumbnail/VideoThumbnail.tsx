import './VideoThumbnail.css';
import { Link } from 'react-router-dom';
import { useEffect } from "react"
import moment from 'moment';

import VideoThumbnailData from '../../../../classes/videoThumbnailData';

function VideoThumbnail(props: any) {
  let video: VideoThumbnailData = props.video

  let maxLen: number = 70
  if(video.title.length > maxLen){
    video.title = video.title.slice(0, maxLen) + "..."
  }

  useEffect(() => {
    let date: Date = new Date(video.date_posted)
    video.date_posted = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()))
  }, [])

  return (
    <div className="VideoPreview col-span-1 flex flex-col">
        <Link to={`/video/${video.id}`}>
          <img src={video.thumbnail_url} alt="" className="Thumbnail aspect-video object-cover rounded"/>
          <div className="flex pt-3">
              <img src="/images/default_profile.png" alt="" className="ChannelImage h-9 aspect-square object-cover rounded-full"/>
              <div className="ml-2 grow">
                  <b className="VideoTitle">{video.title}</b>
                  <p className="ChannelName text-sm text-stone-300">John Doe</p>
                  <div className='flex items-center gap-1.5'>
                    <p className="ViewCount text-sm text-stone-300">{video.views} views</p>
                    <div className='bg-stone-300 aspect-square h-1 rounded-full'></div>
                    <p className="ViewCount text-sm text-stone-300">{moment.utc(video.date_posted).fromNow()}</p>
                  </div>
              </div>
          </div>
        </Link>
    </div>
  );
}

export default VideoThumbnail;
