import { useState, useEffect } from "react"
import { useNavigate } from "react-router-dom"
import axios from "axios"
import { toast } from "react-hot-toast"

import VideoData from "../../classes/videoData"
import { useAuth0 } from "@auth0/auth0-react";

function VideoEditPage(props: any) {

    const { user, isAuthenticated, getAccessTokenSilently } = useAuth0();

    const navigate = useNavigate()

    let splitUrl = window.location.href.split("/")
    let videoID = splitUrl[splitUrl.length-1]
    let apiUrl = `${process.env.REACT_APP_VIDEO_API_BASE_URL}/videos/${videoID}`

    const [video, setVideo] = useState<VideoData>()

    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")

    const handleTitleChange = (e: any) => {
        setTitle(e.target.value)
    }
    const handleDescriptionChange = (e: any) => {
        setDescription(e.target.value)
    }

    const handleSave = async () => {
        const token = await getAccessTokenSilently({
            authorizationParams: {
                audience: process.env.REACT_APP_AUTH0_AUDIENCE
            },
        });

        console.log(token)

        axios.put(`${process.env.REACT_APP_VIDEO_API_BASE_URL}/videos/${videoID}`, 
            {
                title: title,
                description: description,
                video_url: video?.video_url,
                thumbnail_url: video?.thumbnail_url,
                views: video?.views
            },
            {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }).then((response: any) => {
            toast.success("Video updated successfully")
            navigate(`/video/${videoID}`)
        })
    }

    const handleDelete = () => {
        toast.custom((t) => (
            <div
              className={`${
                t.visible ? 'animate-enter' : 'animate-leave'
              } max-w-md w-full bg-white shadow-lg rounded-lg pointer-events-auto flex ring-1 ring-black ring-opacity-5`}
            >
              <div className="flex flex-col w-full p-4 items-center">
                <p className="text-stone-700 text-center">Deleting this video will permanently remove it from Illuminize. This is irreversible. Do you want to continue?</p>
                <div className="flex w-full justify-between mt-3">
                    <button onClick={() => confirmDelete(t.id)} className="'font-semibold bg-red-600 text-white rounded-md px-3 py-2 hover:bg-red-500'">Delete Video</button>
                    <button onClick={() => toast.dismiss(t.id)} className="'font-semibold bg-slate-200 text-stone-900 rounded-md px-3 py-2 hover:bg-gray-200'">Cancel</button>
                </div>
              </div>
            </div>
          ))
    }

    const confirmDelete = (toastId: any) => {
        toast.promise(
            axios.delete(`${process.env.REACT_APP_VIDEO_API_BASE_URL}/videos/${videoID}`).then(() => {
                toast.dismiss(toastId)
                navigate("/")
            }),
            {
                loading: "Deleting...",
                success: "Deleted video successfully",
                error: "Could not delete video"
            }
        )
    }

    useEffect(() => {
        if(props.hamburgerMenuOpen) {
            props.toggleHamburgerMenu()
        }
    
        axios.get(apiUrl).then((response) => {
            let videoData: VideoData = response.data
            let date: Date = new Date(videoData.date_posted)
            videoData.date_posted = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()))
            
            setVideo(videoData)
            setTitle(videoData.title)
            setDescription(videoData.description)
        })
      }, [])

      console.log(video)

    return (
        <div className="w-full h-96 mt-24 px-28 flex flex-col">
            <div className="flex flex-col gap-2">
                <p className="font-semibold text-2xl mb-2">Video Details</p>
                <label htmlFor="title" className='text-white text-sm'>Title:</label>
                <input type="text" name="title" id="title" placeholder='Title...' value={title} className='text-sm px-2 py-2 bg-stone-800 ring-1 ring-stone-600 rounded-md' onChange={handleTitleChange}></input>
                <label htmlFor="description" className='text-white text-sm mt-2'>Description:</label>
                <textarea name="description" id="description" placeholder='Description' value={description} rows={4} className='text-white text-sm px-2 py-2 bg-stone-800 ring-1 ring-stone-600 rounded-md resize-none' onChange={handleDescriptionChange}></textarea>
                <div className="flex items-center justify-between mt-2">
                    <button className="mt-2 px-10 py-1 bg-blue-600 rounded-md hover:bg-blue-500" onClick={handleSave}>Save Changes</button>
                    <button className="px-2 py-1 bg-red-600 rounded-md hover:bg-red-500" onClick={handleDelete}>Delete Video</button>
                </div>
            </div>
        </div>
    );
}

export default VideoEditPage;
