import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './components/App';
import reportWebVitals from './reportWebVitals';
import { Auth0Provider } from '@auth0/auth0-react';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

const authorizationParams = {
  redirect_uri: window.location.origin,
  audience: "https://illuminize.eu.auth0.com/api/v2/",
};

root.render(
  <Auth0Provider
    domain="illuminize.eu.auth0.com"
    clientId="u2Bo2xIVbRkmiEaKqBKpLcF7ozGP6YQE"
    useRefreshTokens={false}
    cacheLocation = "localstorage"
    authorizationParams={authorizationParams}>

    <React.StrictMode>
      <App />
    </React.StrictMode>

  </Auth0Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
