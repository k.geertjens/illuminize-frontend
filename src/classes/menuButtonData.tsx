class MenuButtonData{
    text: string = ""
    link: string = ""
    icon: JSX.Element = <></>

    constructor(text: string, link: string, icon: JSX.Element){
        this.text = text
        this.link = link
        this.icon = icon
    }
}

export default MenuButtonData