class VideoData{
    id: number = -1
    poster_id: number = -1

    video_url: string = "default.m3u8" //setting a default value with .m3u8 extension ensures the video player has working controls
    thumbnail_url: string = ""

    date_posted: Date = new Date("1970-01-01")
    title: string = ""
    description: string = ""
    views: number = 0
    likes: number = 0
    dislikes: number = 0

    constructor(){}
}

export default VideoData