class VideoThumbnailData{
    id: number = -1
    poster_id: number = -1

    thumbnail_url: string = "images/stock_thumbnails/1.jpg"

    date_posted: Date = new Date("1970-01-01")
    title: string = "";
    views: number = 0

    constructor(title: string, thumbnailUrl: string){
        this.title = title
        this.thumbnail_url = thumbnailUrl
    }
}

export default VideoThumbnailData