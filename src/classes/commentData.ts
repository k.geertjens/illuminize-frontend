class CommentData{
    name: string = ""
    date: string = ""
    text: string = ""

    constructor(name: string, date: string, text: string){
        this.name = name
        this.date = date
        this.text = text
    }
}

export default CommentData