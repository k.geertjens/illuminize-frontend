class VideoPostRequest{
    poster_id: number = -1

    video_url: string = ""
    thumbnail_url: string = ""

    title: string = ""
    description: string = ""

    constructor(){}
}

export default VideoPostRequest