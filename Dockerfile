FROM node:lts-alpine3.16

# Copying necessary files
WORKDIR /app
COPY . .

# Build
RUN npm ci

# Run
ENV NODE_ENV production
EXPOSE 3000
CMD ["npm", "start"]